/**
 * Clase que representa las c�lulas en el entorno
 * @author Roberto G�mez Pla
 *
 */
public class Celulas implements Cloneable {

	//Campo representando todo el habitat en el cual viviran las c�lulas.
	private boolean campo[][];
	private int numero_columna,numero_fila;

	/**
	 * Constructor de las c�lulas.
	 * @param numero_fila n�mero de filas del habitat de las c�lulas
	 * @param numero_columna n�mero de clolumnas del habitat de las c�lulas.
	 */
	public Celulas(int numero_fila, int numero_columna) {
		this.numero_columna=numero_columna;
		this.numero_fila=numero_fila;
		this.campo=new boolean[numero_fila][numero_columna];
	}

	/**
	 * M�todo encargado de calcular la siguiente generaci�n
	 * teniendo en cuenta que todo fuera del habitat esta muerto y
	 * si te vas fuera mueres.
	 */
	public void siguienteGeneracion() {
		int celula=0; //Contador de cada c�lula
		int fila;
		int columna;
		boolean[][]copiaCampo=this.copyCampo();	//Copia del Habitat que sera editado, esto se hace por seguridad del otro objeto

		for(fila=0; fila<numero_fila; fila++) {			
			for (columna=0;columna<numero_columna;columna++) {
				//Condiciones especiales para las c�lulas en una de las 4 esquinas:
				if (fila == 0 && columna == 0) {
					if (copiaCampo[fila + 1][columna])
						celula++;
					if (copiaCampo[fila][columna + 1])
						celula++;
					if (copiaCampo[fila + 1][columna + 1])
						celula++;
				} else if (fila == 0 && columna == numero_columna - 1) {
					if (copiaCampo[fila][columna - 1])
						celula++;
					if (copiaCampo[fila + 1][columna])
						celula++;
					if (copiaCampo[fila + 1][columna - 1])
						celula++;
				} else if (fila == numero_fila - 1 && columna == 0) {
					if (copiaCampo[fila - 1][columna + 1])
						celula++;
					if (copiaCampo[fila - 1][columna])
						celula++;
					if (copiaCampo[fila][columna + 1])
						celula++;
				} else if (fila == numero_fila - 1 && columna == numero_columna - 1) {
					if (copiaCampo[fila - 1][columna])
						celula++;
					if (copiaCampo[fila - 1][columna - 1])
						celula++;
					if (copiaCampo[fila][columna - 1])
						celula++;
					//Condiciones especiales para las c�lulas situadas en los l�mites de la matriz:
				} else if (fila == 0 && columna != numero_columna - 1 && columna != 0) {
					if (copiaCampo[fila][columna - 1])
						celula++;
					if (copiaCampo[fila + 1][columna])
						celula++;
					if (copiaCampo[fila][columna + 1])
						celula++;
					if (copiaCampo[fila + 1][columna + 1])
						celula++;
					if (copiaCampo[fila + 1][columna - 1])
						celula++;
				} else if (fila == numero_fila - 1 && columna != numero_columna - 1 && columna != 0) {
					if (copiaCampo[fila - 1][columna])
						celula++;
					if (copiaCampo[fila][columna - 1])
						celula++;
					if (copiaCampo[fila - 1][columna - 1])
						celula++;
					if (copiaCampo[fila][columna + 1])
						celula++;
					if (copiaCampo[fila - 1][columna + 1])
						celula++;
				} else if (fila != numero_fila - 1 && fila != 0 && columna == 0) {
					if (copiaCampo[fila - 1][columna])
						celula++;
					if (copiaCampo[fila][columna + 1])
						celula++;
					if (copiaCampo[fila - 1][columna + 1])
						celula++;
					if (copiaCampo[fila + 1][columna])
						celula++;
					if (copiaCampo[fila + 1][columna + 1])
						celula++;
				} else if (fila != numero_fila - 1 && fila != 0 && columna == numero_columna - 1) {
					if (copiaCampo[fila - 1][columna - 1])
						celula++;
					if (copiaCampo[fila + 1][columna - 1])
						celula++;
					if (copiaCampo[fila][columna - 1])
						celula++;
					if (copiaCampo[fila + 1][columna])
						celula++;
					if (copiaCampo[fila - 1][columna])
						celula++;
					//Condiciones normales para una c�lula:
				} else {								
					if (copiaCampo[fila - 1][columna])
						celula++;
					if (copiaCampo[fila - 1][columna + 1])
						celula++;
					if (copiaCampo[fila - 1][columna - 1])
						celula++;
					if (copiaCampo[fila + 1][columna + 1])
						celula++;
					if (copiaCampo[fila][columna + 1])
						celula++;
					if (copiaCampo[fila + 1][columna - 1])
						celula++;
					if (copiaCampo[fila + 1][columna])
						celula++;
					if (copiaCampo[fila][columna - 1])
						celula++;
				}
				if (copiaCampo[fila][columna]) {
					if (celula == 0 || celula == 1 || celula > 3)
						campo[fila][columna] = false;
				} else {
					if (celula == 3)
						campo[fila][columna] = true;
				}
				celula = 0;

			}
		}
	}

	/**
	 *  M�todo que nos sirve para copiar la matriz.
	 * @return retorna la m�triz copiada
	 */
	private boolean[][] copyCampo()
	{
		boolean[][] copiaCampo = new boolean[numero_fila][numero_columna];

		for (int row = 0; row < numero_fila; row++)
			for (int col = 0; col < numero_columna; col++)
				copiaCampo[row][col] = campo[row][col];

		return copiaCampo;
	}
	/**
	 * Obtienes el habitat de las c�lulas con toda su informaci�n
	 * @return
	 */
	public boolean[][] getCampo()
	{
		return campo;
	}
	/**
	 * M�todo para poner el habitat en el cu�l se desarrollara las c�lulas,
	 * este campo tiene ya c�lulas puestas
	 * @param campo habitat de las c�lulas
	 */
	public void setCampo(boolean[][] campo)
	{
		this.campo = campo;
	}
	/**
	 * M�todo para poder limpiar todo el habitat de las c�lulas.
	 */
	public void clearCampo()
	{
		for (int i = 0; i < campo.length; i++)
			for (int j = 0; j < campo[0].length; j++)
				campo[i][j] = false;	
	}
}
