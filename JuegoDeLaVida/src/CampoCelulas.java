import java.awt.EventQueue;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
/**
 * Clase de swing para representar el entorno
 * @author Roberto G�mez Pla
 *
 */
public class CampoCelulas {

	private JFrame frame;
	private static final int NUMERO_COLUMNAS = 50;
	private static final int NUMERO_FILAS = 40;
	private boolean sigo;
	private Celulas planeta = new Celulas (NUMERO_FILAS, NUMERO_COLUMNAS);
	private JLabel[][] campo = new JLabel[NUMERO_FILAS][NUMERO_COLUMNAS];
	private GridLayout gridLayout = new GridLayout();
	private boolean leftButtonPressed;
	private boolean click = false;

	/**
	 * Ejecuta la aplicaci�n
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CampoCelulas window = new CampoCelulas();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Crea la aplicaci�n
	 */
	public CampoCelulas() {
		initialize();
	}

	/**
	 * Inicializa los contenidos del frame de swing
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1040, 900);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(new Color(64, 224, 208));
		panel.setBorder(BorderFactory.createLineBorder(Color.black));
		panel.setBounds(0, 0, 1040, 800);
		frame.getContentPane().add(panel);
		//Metemos el gridLayout para que se pueda pintar el grid en el panel
		panel.setLayout(gridLayout);
		gridLayout.setColumns(NUMERO_COLUMNAS);
		gridLayout.setHgap(0);
		gridLayout.setRows(NUMERO_FILAS);
		gridLayout.setVgap(0);

		//Inicializamos la matriz campo siendo cada una de sus componentes un Label.
		for (int i = 0; i < campo.length; i++)
		{
			for (int j = 0; j < campo[0].length; j++) {
				campo[i][j] = new JLabel();
				campo[i][j].setBackground(Color.white);
				campo[i][j].setOpaque(true);
				campo[i][j].setBorder(BorderFactory.createLineBorder(Color.black));
				//Creamos cada uno de los Listener
				campo[i][j].addMouseListener(new MouseListener(){

					@Override
					public void mouseClicked(MouseEvent e) {

					}
					//Si un jlabel esta pintado lo pasamos a una nueva matriz como true, matriz que se usara de habitat
					@Override
					public void mouseEntered(MouseEvent e) {
						if (click) {
							Object o = e.getSource();
							if (leftButtonPressed)
								((JLabel) o).setBackground(Color.red);
							else
								((JLabel) o).setBackground(Color.white);
						}
						boolean[][] campo2 = new boolean[NUMERO_FILAS][NUMERO_COLUMNAS];		
						for (int i = 0; i < campo2.length; i++)
						{
							for (int j = 0; j < campo2[0].length; j++) {
								if (campo[i][j].getBackground().equals(Color.red))
									campo2[i][j] = true;
								else
									campo2[i][j] = false;
							}
						}

						planeta.setCampo(campo2);						


					}

					@Override
					public void mouseExited(MouseEvent e) {

					}
					//Si hago click y mantego pinto todas la casillas por las que pase el rat�n de rojo
					@Override
					public void mousePressed(MouseEvent e) {
						click = true;
						Object o = e.getSource();
						if (e.getButton() == MouseEvent.BUTTON1) {
							leftButtonPressed = true;
							((JLabel) o).setBackground(Color.red);
						} else if (e.getButton() == MouseEvent.BUTTON3) {
							leftButtonPressed = false;
							((JLabel) o).setBackground(Color.white);
						}						


					}

					@Override
					public void mouseReleased(MouseEvent e) {
						click = false;	
					}

				});

				panel.add(campo[i][j]);
			}
		}
		JButton btnClearButton = new JButton("Clear");
		JButton btnStopButton = new JButton("Stop");
		btnStopButton.setEnabled(false);
		JButton btnStartButton = new JButton("Start");
		//Listener del bot�n start que crea un hilo para el refresco del swing.
		btnStartButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnStartButton.setEnabled(false);
				btnStopButton.setEnabled(true);
				btnClearButton.setEnabled(false);
				sigo=true;
				Thread background = new Thread(new Runnable(){

					@Override
					public void run() {
						while(sigo) {
							siguienteGeneracion();	//Hago la siguiente generaci�n
							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						btnStartButton.setEnabled(true);
						btnClearButton.setEnabled(true);
					}
				});
				background.start();
			}
		});
		btnStartButton.setBounds(576, 804, 117, 36);
		frame.getContentPane().add(btnStartButton);

		btnStopButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				sigo=false;
				btnStopButton.setEnabled(false);
			}
		});
		btnStopButton.setBounds(285, 804, 124, 36);
		frame.getContentPane().add(btnStopButton);

		//Listener para el bot�n clear, se encarga de matar todas las c�luas del habitat.
		btnClearButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				for (int i = 0; i < campo.length; i++)
					for (int j = 0; j < campo[0].length; j++)
						campo[i][j].setBackground(Color.white);

				planeta.clearCampo();				
			}
		});
		btnClearButton.setBounds(900, 815, 97, 25);
		frame.getContentPane().add(btnClearButton);
	}
	/**
	 * M�todo que realiza la siguiente generaci�n de las c�lulas y lo
	 * pinta en la pantalla
	 */
	public void siguienteGeneracion() {
		planeta.siguienteGeneracion();
		boolean[][] campo2 = planeta.getCampo();
		for (int i = 0; i < campo2.length; i++)
		{
			for (int j = 0; j < campo2[0].length; j++) {
				if (campo2[i][j] == true)
					campo[i][j].setBackground(Color.red);
				else
					campo[i][j].setBackground(Color.white);
			}
		}
	}
}
